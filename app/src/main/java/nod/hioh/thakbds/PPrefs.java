package nod.hioh.thakbds;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;

public class PPrefs extends PreferenceActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String data = getIntent().getDataString();
//        Log.d("intent ", data);
        switch(data) {
            case "OpnPkbd":
                startActivity(new Intent(android.provider.Settings.ACTION_HARD_KEYBOARD_SETTINGS));
                break;
            default:
                break;
        }
    }

}
