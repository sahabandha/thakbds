package nod.hioh.thakbds;

import android.app.Dialog;
import android.content.Context;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Build;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;

public class SKbd extends InputMethodService implements KeyboardView.OnKeyboardActionListener {
    private InputMethodManager mIMM;
    private KeyboardView mIV;
    private int mLastDisplayWidth;
    private LtnKbd mQweKbd;
    private LtnKbd mQwe2Kbd;
    private LtnKbd mTh01Kbd;
    private LtnKbd mTh02Kbd;
    private LtnKbd mTh03Kbd;
    private LtnKbd mCurKbd;

    private void findNowSub(InputMethodSubtype subtype) {
        switch (subtype.getLanguageTag()) {
            case "en-US-02":
                mCurKbd = mQwe2Kbd;
                break;
            case "en-US":
                mCurKbd = mQweKbd;
                break;
            case "th-TH-03":
                mCurKbd = mTh03Kbd;
                break;
            case "th-TH-02":
                mCurKbd = mTh02Kbd;
                break;
            case "th-TH":
            default:
                mCurKbd = mTh01Kbd;
                break;
        }
    }

    private void SetNowSub(InputMethodSubtype subtype) {
        switch (subtype.getLanguageTag()) {
            case "en-US-02":
                setLatinKeyboard(mQwe2Kbd);
                break;
            case "en-US":
                setLatinKeyboard(mQweKbd);
                break;
            case "th-TH-03":
                setLatinKeyboard(mTh03Kbd);
                break;
            case "th-TH-02":
                setLatinKeyboard(mTh02Kbd);
                break;
            case "th-TH":
            default:
                setLatinKeyboard(mTh01Kbd);
                break;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mIMM = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
    }

    Context getDisplayContext() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return this;
        }
        final WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        return createDisplayContext(wm.getDefaultDisplay());
    }

    @Override
    public void onInitializeInterface() {
        final Context displayContext = getDisplayContext();

        if (mQweKbd != null) {
            int displayWidth = getMaxWidth();
            if (displayWidth == mLastDisplayWidth) return;
            mLastDisplayWidth = displayWidth;
        }

        mQweKbd = new LtnKbd(displayContext, R.xml.qwerty);
        mQwe2Kbd = new LtnKbd(displayContext, R.xml.qwerty2);
        mTh01Kbd = new LtnKbd(displayContext, R.xml.thai);
        mTh02Kbd = new LtnKbd(displayContext, R.xml.thai2);
        mTh03Kbd = new LtnKbd(displayContext, R.xml.thai3);
    }

    @Override
    public View onCreateInputView() {
        mIV = (KeyboardView) getLayoutInflater().inflate(R.layout.input, null);
        mIV.setOnKeyboardActionListener(this);
        mIV.setPreviewEnabled(false);
        SetNowSub(mIMM.getCurrentInputMethodSubtype());
        return mIV;
    }

    private void setLatinKeyboard(LtnKbd nextKeyboard) {
        mIV.setKeyboard(nextKeyboard);
    }

    @Override
    public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);
        findNowSub(mIMM.getCurrentInputMethodSubtype());
        mCurKbd.setImeOptions(getResources(), attribute.imeOptions);
    }

    @Override
    public void onFinishInput() {
        super.onFinishInput();

        findNowSub(mIMM.getCurrentInputMethodSubtype());
        if (mIV != null) {
            mIV.closing();
        }
    }

    @Override
    public void onStartInputView(EditorInfo attribute, boolean restarting) {
        super.onStartInputView(attribute, restarting);
        setLatinKeyboard(mCurKbd);
        mIV.closing();
        final InputMethodSubtype subtype = mIMM.getCurrentInputMethodSubtype();
    }

    public void onCurrentInputMethodSubtypeChanged(InputMethodSubtype subtype) {
        SetNowSub(subtype);
    }
    public void onKey(int primaryCode, int[] keyCodes) {
        if (primaryCode == LtnKbd.KEYCODE_INPUT_SWITCH) {
            InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
            imeManager.showInputMethodPicker();
        } else if (primaryCode == LtnKbd.KEYCODE_LANGUAGE_SWITCH) {
            handleLanguageSwitch();
        } else {
            handleCharacter(primaryCode);
        }
    }

    public void onText(CharSequence text) {
    }


    private void handleCharacter(int primaryCode) {
        if (isInputViewShown()) {
            if (mIV.isShifted()) {
                primaryCode = Character.toUpperCase(primaryCode);
            }
        }
        getCurrentInputConnection().commitText(String.valueOf((char) primaryCode), 1);
    }

    private IBinder getToken() {
        final Dialog dialog = getWindow();
        if (dialog == null) {
            return null;
        }
        final Window window = dialog.getWindow();
        if (window == null) {
            return null;
        }
        return window.getAttributes().token;
    }

    private void handleLanguageSwitch() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            mIMM.switchToNextInputMethod(getToken(), true /* onlyCurrentIme */);
            SetNowSub(mIMM.getCurrentInputMethodSubtype());
        }
    }

    public void swipeRight() {
    }

    public void swipeLeft() {
    }

    public void swipeDown() {
    }

    public void swipeUp() {
    }

    public void onPress(int primaryCode) {
    }


    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_SHIFT_RIGHT:
            case KeyEvent.KEYCODE_SHIFT_LEFT:
                if (event.isCtrlPressed()) {
                    handleLanguageSwitch();
                }
                return false;
            default:
                return false;
            //return super.onKeyUp(keyCode, event);
        }
    }

    public void onRelease(int primaryCode) {
    }
}
