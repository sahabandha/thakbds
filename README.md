### For sa*msu** users, see note below.
Download : [https://gitlab.com/sahabandha/thakbds/-/releases]

# More Thai Physical Keyboards for Android

Support:

- Thai Manoonchai V.1
- Thai Manoonchai V.0.2
- Thai Manoonchai V.1 Colemak (with Capslock as Backspace)
- Thai Manoonchai V.1 Colemak DH mod
- Thai Manoonchai-WittNV V.1

from HudchewMan:
- Thai Kedmanee
- Thai Kedmanee (non-Shiftlock)
- Thai Kedmanee Writer
- Thai Kedmanee Writer (non-Shiftlock)
- Thai Pattachote
- Thai Pattachote (non-Shiftlock)
- Thai Pattachote Writer
- Thai Pattachote Writer (non-Shiftlock)
- Thai Manoonchai Writer
- Thai Manoonchai Writer (non-Shiftlock)

Writer version is the modified-layout from original version by replace some key with character that often use in novel work and add AltGr layout.

ผังแป้น writer คือรุ่นที่ดัดแปลงจากผังแป้นเดิมโดยนำอักขระที่มีการใช้งานบ่อยในงานพิมพ์นิยายและบทความมาใส่แทนปุ่มเดิมที่ใช้งานน้อย รวมถึงเพิ่มปุ่ม AltGr เพื่อลดการสลับภาษาไปมา

## For sa*su** user
Due s*msu** lacks of a separate hardware keyboard layout changer. 

### better method for using in smasnug dex mode
NOTE: 
1. Use factory s*m**ng keyboard as default (Due we cannot change ime to another better one.)
1. Add language `English (US)` and `English (UK)`   
(Due some keyboard / O_e UI versions, we are not able to change layout for Thai, we will change layout for English instead. If you don't have this issue, you can set it to Thai.)
1. Set physical keyboard in English (UK) to your flavor. (see screenshot below)

![How to set physical keyboard](doc/example.png)

### old method
You can use the pseudo on-screen keyboard (appears as MTPK Pseudo; included in this app) to map 1:1 hardware keyboard. Change keyboard layout by pressing ctrl-shift.
