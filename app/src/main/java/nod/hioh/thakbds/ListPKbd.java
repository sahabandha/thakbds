package nod.hioh.thakbds;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.preference.PreferenceCategory;

import androidx.preference.PreferenceScreen;

import android.provider.Settings;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import java.util.List;

public class ListPKbd extends AppCompatActivity {
    private static InputMethodManager mImm;
    private static InputMethodInfo mImi;

    private ListView l;


    public static boolean createPrefs(final Context context, final PreferenceScreen prefScreen) {
        mImm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        mImi = getMyImi(context, mImm);
        if (mImi == null || mImi.getSubtypeCount() <= 1) {
            return false;
        }

        PreferenceCategory ctg_psd_kbd = new PreferenceCategory(prefScreen.getContext());
        ctg_psd_kbd.setTitle(R.string.psd_kbd);
        prefScreen.addPreference(ctg_psd_kbd);


        Preference mOpnLang = new Preference(context);
        mOpnLang.setTitle(R.string.opn_psd_kbd);
        mOpnLang.setSummary(R.string.opn_psd_kbd_sum);
        mOpnLang.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                context.startActivity(new Intent(android.provider.Settings.ACTION_INPUT_METHOD_SETTINGS));
                return true;
            }
        });
        ctg_psd_kbd.addPreference(mOpnLang);
        Preference mSubtype = new Preference(context);
        mSubtype.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                final Intent intent = new Intent(Settings.ACTION_INPUT_METHOD_SUBTYPE_SETTINGS);
                intent.putExtra(Settings.EXTRA_INPUT_METHOD_ID, mImi.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
                return true;
            }
        });
        mSubtype.setTitle(R.string.subt);
        mSubtype.setSummary(R.string.subt_sum);
        ctg_psd_kbd.addPreference(mSubtype);


        Preference mPsdSum = new Preference(context);
        mPsdSum.setSummary(R.string.psd_kbd_sum);
        mPsdSum.setSelectable(false);
        mPsdSum.setPersistent(false);
        ctg_psd_kbd.addPreference(mPsdSum);

        PreferenceCategory ctg_pkbd = new PreferenceCategory(prefScreen.getContext());
        ctg_pkbd.setTitle(R.string.pkbd);
        prefScreen.addPreference(ctg_pkbd);

        Preference mOpnPkbd = new Preference(context);
        mOpnPkbd.setTitle(R.string.opn_pkbd);
        mOpnPkbd.setSummary(R.string.opn_pkbd_sum);
        mOpnPkbd.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                context.startActivity(new Intent(android.provider.Settings.ACTION_HARD_KEYBOARD_SETTINGS));
                return true;
            }
        });
        ctg_pkbd.addPreference(mOpnPkbd);

        return true;
    }

    private static InputMethodInfo getMyImi(Context context, InputMethodManager imm) {
        final List<InputMethodInfo> imis = imm.getInputMethodList();
        for (int i = 0; i < imis.size(); ++i) {
            final InputMethodInfo imi = imis.get(i);
            if (imis.get(i).getPackageName().equals(context.getPackageName())) {
                return imi;
            }
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.settings, new SettingsFragment()).commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        l = findViewById(R.id.kbdslist);
        String[] Kbds =  {
                getString(R.string.kbd_tha_manoonchai),
                getString(R.string.kbd_tha_manoonchai_0_2),
                getString(R.string.kbd_tha_manoonchai_colemak),
                getString(R.string.kbd_tha_manoonchai_colemak_dh),
                getString(R.string.kbd_tha_manoonchai_wittnv),
                getString(R.string.kbd_tha_manoonchai_writer_1),
                getString(R.string.kbd_tha_manoonchai_writer_1_non_shiftlock),
                getString(R.string.kbd_tha_kedmanee),
                getString(R.string.kbd_tha_kedmanee_non_shiftlock),
                getString(R.string.kbd_tha_kedmanee_writer_1),
                getString(R.string.kbd_tha_kedmanee_writer_1_non_shiftlock),
                getString(R.string.kbd_tha_pattachote),
                getString(R.string.kbd_tha_pattachote_non_shiftlock),
                getString(R.string.kbd_tha_pattachote_writer_1),
                getString(R.string.kbd_tha_pattachote_writer_1_non_shiftlock),
        };
        ArrayAdapter<String> arr;
        arr = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, Kbds);
        l.setAdapter(arr);
        l.setEnabled(false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

            final Context context = getActivity();
            setPreferenceScreen(getPreferenceManager().createPreferenceScreen(context));
            createPrefs(context, getPreferenceScreen());
        }
    }

}
