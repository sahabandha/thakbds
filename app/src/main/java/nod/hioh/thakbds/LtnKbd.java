/*
 * Copyright (C) 2008-2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nod.hioh.thakbds;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.inputmethodservice.Keyboard;

public class LtnKbd extends Keyboard {

    public static final int KEYCODE_LANGUAGE_SWITCH = -101;
    public static final int KEYCODE_INPUT_SWITCH = -100;

    private Key mEnterKey;
    private Key mLanguageSwitchKey;
    private Key mSavedLanguageSwitchKey;

    public LtnKbd(Context context, int xmlLayoutResId) {
        super(context, xmlLayoutResId);
    }

    @Override
    protected Key createKeyFromXml(Resources res, Row parent, int x, int y, XmlResourceParser parser) {
        Key key = new Key(res, parent, x, y, parser);
        if (key.codes[0] == Keyboard.KEYCODE_DONE) {
            mEnterKey = key;
        } else if (key.codes[0] == KEYCODE_LANGUAGE_SWITCH) {
            mLanguageSwitchKey = key;
            mSavedLanguageSwitchKey = new Key(res, parent, x, y, parser);
        }
        return key;
    }

    void setImeOptions(Resources res, int options) {
        if (mEnterKey == null) {
            return;
        }

    }
}
